name = "Adrian Maraasin"
age = 29
occupation = "Civil Engineer"
movie = "Top Gun Maverick"
rating = 99.8 

print(f"I am {name}, and I am {age} years old, I work as a {occupation}, and my rating for {movie} is {rating} %.")

num1 = 1
num2 = 150
num3 = 10

print(num1*num2)
print(num1 < num3)
print(num3 + num2)